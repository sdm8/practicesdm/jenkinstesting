const express=require('express');
const db=require('../db');
const utils=require('../utils')
const router=express.Router();
router.post('/addcar',(req,res)=>{
    const{name,model,price,carImage}=req.body;
    const statement=`insert into carTB(name,model,price,carImage) values(?,?,?,?);`;
    db.Pool.query(statement,[name,model,price,carImage],(err,data)=>{
        res.send(utils.createResult(err,data));
    })
    
})
router.get('/getallcar',(req,res)=>{
    const statement=`select * from carTB`;
    db.Pool.query(statement,(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})
router.delete('/deletecar/:id',(req,res)=>{
    const {id}=req.params;
    const statement=`delete from carTB where id=?;`;
    db.Pool.query(statement,[id],(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})
module.exports=router;