const express=require('express')
const db=require('../db')
const utils=require('../utils');
const router=express.Router();

router.post('/signin',(req,res)=>{
    const{email,password}=req.body;
    const statement=`select * from userTB where email=? and password=? ; `;
    db.Pool.query(statement,[email,password],(err,data)=>{
        res.send(utils.createResult(err,data));
    })
})
module.exports=router;