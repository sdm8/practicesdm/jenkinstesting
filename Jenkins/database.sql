drop database  if exists lastYear;
create database lastYear;

use lastYear;

create table employeeTB(
id int primary key auto_increment,
name varchar(50),
department varchar(50),
salary decimal(5,2),
employeeImage varchar(200)
);
create table userTB(
userid int primary key auto_increment,
companyName varchar(50),
email varchar(50),
password varchar(100)
);

insert into userTB(companyName,email,password) values('Honda','user1@.com','user1');